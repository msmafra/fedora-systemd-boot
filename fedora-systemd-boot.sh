#!/usr/bin/env bash
#{{{ Bash Settings
set -o errexit
set -o errtrace
set -o nounset
set -o pipefail
set -o xtrace
#
#}}}
#################################################################
#                                                               #
#                  Fedora Systemd-Boot Installer                #
#                         Version: 0.0.1                        #
#                       Copyright (C) 2020                      #
#           Marcelo dos Santos Mafra <msmafra@gmail.com>        #
#       Licensed under the GNU General Public License v3.0      #
#                                                               #
#        https://github.com/msmafra/fedora-systemd-boot.sh      #
#                                                               #
#################################################################
# Systemd-boot install on Fedora 32
# https://kowalski7cc.xyz/blog/systemd-boot-fedora-32
#
#{{{ Primary Variables
# readonly SCRIPT_TITLE="Fedora Systemd boot install"
# readonly SCRIPT_VERSION="0.0.1"
# readonly SCRIPT_FILE=$(basename "${0}")
# readonly SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
#IFS=$'\t\n' # Split on newlines and tabs (but not on spaces)
IFS="$(printf '\n\t')"
#}}}
#{{{ main call
function main() {
    trap -p exitStageLeft EXIT ERR # Elegant exit
    checkSudo
    isEfi
    #setSystemdBoot
    #installSystemdBoot
    checkSystemdBoot
}
#}}}
#{{{ Primary Functions
function exitStageLeft() {
    # ? gets the exit code number
    printf "\n%s" "So exit, Stage Left! ${?}"
}

function checkSudo() {
    local wfxThisUser
    wfxThisUser=$(\id --user --name)
    if [[ "${EUID}" != 0 ]];then
        printf "%s, %s\n" "${wfxThisUser}" "run me with sudo or doas, pleeeese."
        # exit "${?}"
        exit 1
    fi
}

function errMsg() {
    local theMessage
    local theTimeStamp
    # Prints error messages
    theMessage="${1}"; shift
    theTimeStamp="$(date +'%Y-%m-%dT%H:%M:%S%z')"
    # printf "%s [$(date +'%Y-%m-%dT%H:%M:%S%z')]: ${@}" >&2
    printf "%s %s" "[${theTimeStamp}]:" "${theMessage}"
}
#}}}
#{{{ Secondary Functions
function isEfi() {
    printf "%s" "Check is the system is running in EFI."
    # test -d /sys/firmware/efi && echo EFI || echo Legacy
    test -d /sys/firmware/efi && printf "%s" "EFI" || printf "%s" "Legacy" && exit 2
}

function setSystemdBoot() {
    # First of all, we need to move the ESP partition from /boot/efi to /efi
    # You can move it or just create a secondary mounting point
    printf "%s" "# Creating the folder for EFI mount point"
    \mkdir /efi
    printf "%s" "Backing /etc/fstab up..."
    \cp --verbose /etc/fstab{,.backup-"$(date +%Y%m%d)"} # (Make a backup first if you are unsure)
    printf "%s\n" "# You can use Vim or Nano to edit the fstab file manually or "
    printf "%s\n" "alow the next command sed -i 's+/boot/efi+/efi+' /tmp/fstab to make the modification "
    printf "%s\n" "Your file now:"
    \cat /etc/fstab
    printf "%s\n" "Your file will look like this after the change by sed:"
    sed --expression 's+/boot/efi+/efi+' /tmp/fstab
    printf "%s %s" "Do you want sed to make the change? Yes for sed or No for your editor" "${EDITOR}"
    select choice in Yes No
    do
        case "${choice}" in
                # Two case values are declared here for matching
            "Yes")
                # sed --in-place 's+/boot/efi+/efi+' /tmp/fstab
                sed --expression 's+/boot/efi+/efi+' /tmp/fstab
                break
                ;;
            "No")
                printf "%s" "You will be put on the correct line"
                "${EDITOR}" +"$(\grep --extended-regexp --recursive --line-number "\/boot\/efi" /etc/fstab \
                | \awk --field-separator ":" '{print $1}')" /etc/fstab
                
                printf "%s" "# Edit the line by entering in \"insert mode\" by pressing the \"i\" button"
                printf "%s\n" "UUID=xxxx-xxxx    /boot/efi    vfat    umask=0077,shortname=winnt 0 2"
                printf "%s\n" "to:"
                printf "%s\n" "UUID=xxxx-xxxx    /efi    vfat    umask=0077,shortname=winnt 0 2"
                printf "%s" "Press the esc button, followed by \":wq\""
                printf "%s" "If you edited correctly you should be able to run successfully the following commands:"
                break
                ;;
                # Matching with invalid data
            *)
                echo "Invalid entry."
                ;;
        esac
    done

    \umount --verbose /boot/efi
    \mount --verbose /efi

    printf "%s" "Install systemd-boot"
    \mkdir --verbose /efi/"$(\cat /etc/machine-id)"
    #\dnf remove grubby grub2\* shim\* memtest86\ && \rm -rf /boot/grub2 && \rm -rf /boot/loader
    printf "%s" "Do not reboot after this"
    \cut --delimiter=' ' --fields=2- /proc/cmdline | tee --ignore-interrupts  /etc/kernel/cmdline
}

function installSystemdBoot() {
    \bootctl install
    \kernel-install --verbose add "$(uname -r)" /lib/modules/"$(uname -r)"/vmlinuz
    \dnf reinstall kernel-core

    printf "%s" "Now you can reboot, and you should boot normally using systemd-boot"
    # If you kept grub you will have to change the boot order inside your UEFI settings
    # or use the Boot Menu key of your motherboard to choose it and test it.
}

function checkSystemdBoot() {
    printf "%s" "Check if it's installed in the ESP"
    \bootctl is-installed
    printf "%s" "You can verify everything working correctly with"
    \bootctl
    printf "%s" "List boot loader entries"
    \bootctl list
}
